import { loginApi, getInfo, getAllInfo, deleteApi, addUserApi, editUserApi } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    userList: [],
    userInfo: {
      id: 0,
      name: '',
      account: '',
      active: 0
    }
  }
}

const state = getDefaultState()

const mutations = {
  resetState: (state) => {
    Object.assign(state, getDefaultState())
  },
  setToken: (state, token) => {
    state.token = token
  },
  setAccount: (state, account) => {
    state.userInfo.account = account
  },
  setUser: (state, info) => {
    state.userInfo = info
  },
  setUserList: (state, list) => {
    state.userList = list
  },
  setDelete: (state, index) => {
    state.userList.splice(index, 1)
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      const { account, password } = userInfo
      const data = {
        account: account.trim(),
        password: password
      }
      loginApi(data)
        .then((res) => {
          if (res.code === 200001) {
            commit('setToken', res.result.token)
            setToken(res.result.token)
            commit('setUser', res.result.user)
            resolve()
          }
        }).catch(error => {
          reject(error)
        })
    })
  },

  addUser({ commit }, userInfo) {
    addUserApi(userInfo)
      .then((res) => {
        if (res.code === 200001) {
          getAllInfo().then(response => {
            commit('setUserList', response.result)
          })
        }
      })
  },

  editUser({ commit }, userInfo) {
    editUserApi(userInfo)
      .then((res) => {
        if (res.code === 200001) {
          getAllInfo().then(response => {
            commit('setUserList', response.result)
          })
        }
      })
  },

  // get user info
  getInfo({ commit, state }) {
    // return new Promise((resolve, reject) => {
    getInfo(state.userInfo.account)
      .then(response => {
        commit('setUser', response.result)
        // resolve(data)
      })
    // .catch(error => {
    //   reject(error)
    // })
    // })
  },

  getUserList({ commit, state }) {
    getAllInfo().then(response => {
      commit('setUserList', response.result)
    })
  },

  deleteUser({ commit }, data) {
    deleteApi({ account: data.account }).then(response => {
      if (response.code === 200001) {
        commit('setDelete', data.index)
      }
    })
  },

  // user logout
  logout({ commit, state }) {
    // return new Promise((resolve, reject) => {
    // logout(state.token).then(() => {
    removeToken() // must remove  token  first
    resetRouter()
    commit('resetState')
    //   resolve()
    // }).catch(error => {
    //   reject(error)
    // })
    // })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('resetState')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

